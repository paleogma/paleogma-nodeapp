const ip = require("ip");

const local = true;

const conf = {
  ip: ip.address(),
  port: "1337"
};

/* Switch all IP to target local */
if (local) {
  conf.ip = "127.0.0.1";
}

module.exports = conf;