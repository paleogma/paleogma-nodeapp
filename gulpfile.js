/**
 * Created by Sko on 20/10/2017.
 */

var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

/** ******************************* **/
/**            [node]:dev           **/
/** ******************************* **/

const clearconsole = function() {
  process.stdout.write('\x1Bc');
};

gulp.task('nodemon', function () {
  clearconsole();
  nodemon({
    script: 'src/App.js'
  })
});