/**
 * Created by Sko on 13/04/2018.
 */

const conf = require("./../conf");
const Utils = require('./Utils');
const HttpServer = require("./components/network/HttpServer");
const WebSocketServer = require("./components/network/WebSocketServer");
const WebSocket = require("ws");

class App {
  constructor() {
    Utils.clearConsole();

    this.ns = new HttpServer({
      name: "Paléogma",
      ip: conf.ip,
      port: conf.port,
      log: true
    });

    this.wss = new WebSocketServer({
      name: "Paléogma",
      server: this.ns.server,
      log: true
    });

    // this.initSocketClient();
  }

  initWebSocketServer() {

    // this.wss.on('connection', (wsc, req, res) => {
    //   console.log("new connection");
    //   // console.log(res);
    //   // ws.on('message', function incoming(message) {
    //   //   console.log('received: %s', message);
    //   // });
    //   //
    //   const source = "salut";
    //   const buffer = new Buffer(source);
    //
    //   wsc.send(JSON.stringify({
    //     data: 1
    //   }));
    //
    //   // wsc.on("")
    // });
  }

  initSocketClient() {
    this.ws = new WebSocket(`ws://${conf.ip}:${conf.port}`, {
      origin: `http://${conf.ip}:${conf.port}`
    });

    this.ws.on('open', () => {
      console.log('connected');
      const data = {
        data1 : 1, data2: 2, data3: 3
      };
      const eventName = "testEvent";
      const payload = {
        event: eventName,
        data
      };
      this.ws.send(JSON.stringify(payload));
    });
    //
    this.ws.on('close', function close() {
      console.log('disconnected');
    });
    //
    this.ws.on('message', json => {
      // console.log(buffer.toString());
      // console.log(JSON.parse(json));
      // const mask = crypto.randomBytes(4);
      // console.log(bufferUtil.unmask(buffer, mask));
      // console.log(`Roundtrip time: ${Date.now() - data} ms`);
      //
      // setTimeout(function timeout() {
      //   ws.send(Date.now());
      // }, 500);
      // console.log(data);
    });
  }
}

new App();