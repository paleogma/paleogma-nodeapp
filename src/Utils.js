/**
 * Created by Sko on 13/04/2018.
 */

const clearConsole = function() {
  process.stdout.write('\x1Bc');
};

module.exports = {
  clearConsole
};