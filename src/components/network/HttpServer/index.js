/**
 * Created by Sko on 20/05/2017.
 */

const chalk = require('chalk');
// const express = require('express')();
// express.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });
// express.get('/', function(req, res, next) {
//   // Handle the get for this route
//   next();
// });
const http = require('http');

class HttpServer {

  static setProps(props) {
    const defaultProps =  {
      name: "NodeServer",
      ip: "127.0.0.1",
      port: "8080",
      log: false
    };

    return {
      ...defaultProps,
      ...props
    }
  }

  constructor(props) {
    this.props = HttpServer.setProps(props);

    this.server = http.createServer();
    this.server.listen(this.props.port, this.props.ip);

    this.log(`${chalk.cyan(this.props.ip)}${chalk.grey(":")}${chalk.magenta(this.props.port)} ${chalk.grey(this.props.name)}`);
    this.log(`Ready ${chalk.green("✓")}`);
  }

  log(toBeLogged) {
    if (this.props.log) {
      console.log(`${chalk.grey("[HttpServer]")} ${toBeLogged}`);
    }
  }
}

module.exports = HttpServer;
