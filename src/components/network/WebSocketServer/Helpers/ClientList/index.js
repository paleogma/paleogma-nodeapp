const _ = require('lodash');
const colors = require('colors');
const ClientTypes = require('./../ClientTypes');

class ClientList {

  constructor() {
    this.initTypes(ClientTypes);
  }

  initTypes(typeList) {
    _.forEach(typeList, (function (value, key) {
      this[value] = {};
    }).bind(this));
  }

  isIdTaken(id) {
    return _.indexOf(this.idList(), id) !== -1;
  }

  getTypes() {
    return _.keys(this);
  }

  idList() {
    let a = [];
    _.forEach(this, (function (value) {
      a = _.concat(a, _.keys(value));
    }));
    return a;
  }

  clientLog() {
    const width = 40;
    separator();
    header();
    timer();
    separator();
    blocks.call(this);

    function centerContent(rBorder = '|', str, lBorder = '|', strLength) {
      let std = lBorder;
      let fillLgt = width - strLength;
      let before = fillLgt / 2;
      let after = fillLgt / 2;
      if(width % 2 === 0 && strLength % 2 !== 0 || width % 2 !== 0 && strLength % 2 === 0) {
        before = fillLgt / 2 - 0.5;
        after = fillLgt / 2 + 0.5;
      }
      for (let i = 0; i < before; i++) {
        std += ' ';
      }
      std += str;
      for (let i = 0; i < after; i++) {
        std += ' ';
      }
      std += rBorder;
      return std;
    }

    function fillContent(lBorder = '|', str, rBorder = '|', strLength) {
      let std = lBorder + ' ' + str;
      let fillLgt = width - strLength - 1;
      for (let i = 0; i < fillLgt; i++) {
        std += ' ';
      }
      std += rBorder;
      return std;
    }

    function separator() {
      let std = "[";
      for(let i = 0; i < width; i++) {
        std += '-';
      }
      std += ']';
      console.log(std);
    }

    function header() {
      const std = "List of connected clients by type";
      console.log(centerContent('|', colors.green(std), '|', std.length));
    }

    function timer() {
      const date = new Date();
      const dateFormatted = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
      // console.log(logH + colors.green(' --:: Client list ::--' + ' => ' + dateFormatted));
      console.log(centerContent('|', colors.grey(dateFormatted), '|', dateFormatted.length));
    }

    function blocks() {
      _.forEach(this, function (value, key) {
        const std = key + ' : [' + _.keys(value).length + ']';
        console.log(fillContent("|", std.replace(key, colors.magenta(key)), "|", std.length));
        console.log(fillContent("|", '', '|', ''.length));
        _.forEach(value, function (value, key) {
          const std2 = '=> ' + key;
          console.log(fillContent("|", std2.replace(key, colors.cyan(key)), '|', std2.length));
        });
        separator();
      });
    }
  }
}

module.exports = ClientList;
