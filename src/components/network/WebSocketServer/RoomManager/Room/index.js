/**
 * Created by Sko on 18/04/2018.
 */

class Room {
  static setProps(props) {
    const defaultProps = {
      name: null
    };

    return {
      ...defaultProps,
      ...props
    }
  }

  constructor(props) {
    this.props = Room.setProps(props);
    this.socketCollection = {};
  }

  push(socketId) {
    if(!this.socketCollection[socketId]) {
      this.socketCollection[socketId] = socketId;
    }
  }

  remove(socketId) {
    if(this.socketCollection[socketId]) {
      delete this.socketCollection[socketId];
    }
    return socketId;
  }

  logSocketCollection() {
    console.log(this.props.name);
  }
}

module.exports = Room;