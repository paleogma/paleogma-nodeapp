/**
 * Created by Sko on 18/04/2018.
 */

const Room = require('./Room');

class RoomManager {
  static setProps(props) {
    const defaultProps = {
      log: false
    };

    return {
      ...defaultProps,
      ...props
    }
  }

  constructor(props) {
    this.props = RoomManager.setProps(props);
    this.roomCollection = {};
    this.createRoom("global");
  }

  createRoom(name) {
    if(!this.roomCollection[name]) {
      this.roomCollection[name] = new Room({
        name
      });
    }
  }

  pushToRoom(roomName, socketId) {
    this.roomCollection[roomName].push(socketId);
  }

  destroyRoom(name) {
    if(this.roomCollection[name]) {
      delete this.roomCollection[name];
    }
  }

  logRoomCollection() {
    if (this.props.log) {
      Object.keys(this.roomCollection).forEach(key => {
        this.roomCollection[key].logSocketCollection();
      });
    }
  }

  log(toBeLogged) {
    if (this.props.log) {
      console.log(`${chalk.grey("[RoomManager]")} ${toBeLogged}`);
    }
  }
}

module.exports = RoomManager;