

const colors = require('colors');
const _ = require('lodash');
const ClientList = require('./Helpers/ClientList');
const ct = require('./Helpers/ClientTypes');

const WebSocket = require('ws');
const bufferUtil = require('bufferutil');
const crypto = require('crypto');
const chalk = require('chalk');

class WebSocketServer {

  static setProps(props) {
    const defaultProps = {
      name: "WebSocketServer",
      server: null,
      log: false,
      events: {}
    };

    return {
      ...defaultProps,
      ...props
    }
  }

  constructor(props) {
    this.props = WebSocketServer.setProps(props);

    /* Creating WebSocket Server */
    this.socket = new WebSocket.Server({
      server: this.props.server
    });

    this.log(`Ready ${chalk.green("✓")}`);

    this.bindCustomEvents();
    this.bindNativeEvents();
  }

  log(toBeLogged) {
    if (this.props.log) {
      console.log(`${chalk.grey("[WebSocketServer]")} ${toBeLogged}`);
    }
  }

  bindNativeEvents() {
    this.socket.on('connection', (ws, req) => {
      console.log("new connection");
      // const location = url.parse(req.url, true);
      // // You might use location.query.access_token to authenticate or share sessions
      // // or req.headers.cookie (see http://stackoverflow.com/a/16395220/151312)
      //
      // ws.on('message', function incoming(message) {
      //   console.log('received: %s', message);
      // });
      //
      // ws.send('something');
    });
  }

  bindCustomEvents() {

    // this.clientList = new ClientList();

    // this.bindEvents();

    this.on('connection', this.handleConnection.bind(this));

    this.on("test", args => {
      console.log("teeeeest ", args);
    });
  }

  on(event, callback) {
    if(!this.eventCollection) {
      this.eventCollection = {};
    }
    this.eventCollection[event] = callback;
  }

  handle(event, args) {
    if(this.eventCollection && this.eventCollection[event]) {
      this.eventCollection[event](args);
    }
  }

  // bindEvents() {
  //   this.on('connection', this.handleConnection.bind(this));
  // }

  bindSocketType({ id, type, name }) {

    /* Transfer from pending to good section */
    this.clientList[type][id] = {};
    this.clientList[type][id].socket = this.clientList[ct._PENDING_][id].socket;
    this.clientList[type][id].name = name;

    this.bindSocketEvents({ id, type });

    /* Sending Confirmation */
    this.clientList[type][id].socket.emit('socketAdded');
    this.log(id + ' answered with type ' + type + ' - It is a ' + name + ' - state : Binded & Ready to communicate\n');
  }

  bindSocketEvents({ id, type }) {
    const socket = this.clientList[type][id].socket;
    /* Binding global events */
    socket.on('disconnect', this.removeClient.bind(this, { id, type }));

    /* Binding specific events */
    switch (type) {
      case ct._DEFAULT_ : {
        socket.on('test', this.handleTest.bind(this));
        break;
      }
      case ct._WEB_ : {
        break;
      }
      case ct._GAME_ : {
        break;
      }
      case ct._IOS_ : {
        break;
      }
    }
  };

  removeClient({ id, type }) {
    this.log('Client : ' + id + ' - ' + this.clientList[type][id].name + ' (' + type + ') disconnected and has been removed from list\n');
    delete this.clientList[type][id];
    this.clientList.clientLog();
  }

  handleConnection(socket) {
    console.log(socket.id);
    // const id = socket.id;
    //
    // this.clientList[ct._PENDING_][id] = {};
    // this.clientList[ct._PENDING_][id].socket = socket;
    // this.clientList.clientLog();
    // this.clientList[ct._PENDING_][id].socket.on('socketType', this.handleSocketType.bind(this));
    // this.clientList[ct._PENDING_][id].socket.emit('requestType', id);
    this.log('ID ' + id + ' generated and sent for confirmation, waiting for an answer\n');
  }

  handleSocketType({ id, detail }) {
    const { type, name } = detail;

    /* Type verification */
    if (_.findIndex(type, this.clientList.getTypes())) {
      this.bindSocketType({ id, type, name });
    } else {
      this.log(id + '-' + name + ' :: Type unknown :: connexion killed');
    }
    delete this.clientList[ct._PENDING_][id];
    this.clientList.clientLog();

    // @todo Ajouter un check permanent (~5 sec) de ping pour detecter les deconnexions
    // @todo Gérer les déconnexions manuelles + reconnexions
  }

  handleTest({ id, detail }) {
    if (this.props.events.onTest) {
      this.props.events.onTest();
    }
  }

  emitJoystickChange(data) {
    // console.log(data);
    _.forIn(this.clientList[ct._IOS_], v => {
      v.socket.emit('joystickChange', {
        detail: data
      })
    });
  }
}

module.exports = WebSocketServer;
