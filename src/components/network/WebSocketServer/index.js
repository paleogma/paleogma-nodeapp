const WebSocket = require('ws');
const bufferUtil = require('bufferutil');
const crypto = require('crypto');
const chalk = require('chalk');
const RoomManager = require('./RoomManager');

class WebSocketServer {

  static setProps(props) {
    const defaultProps = {
      name: "WebSocketServer",
      server: null,
      log: false,
      events: {}
    };

    return {
      ...defaultProps,
      ...props
    }
  }

  constructor(props) {
    this.props = WebSocketServer.setProps(props);

    /* Creating WebSocket Server */
    this.socket = new WebSocket.Server({
      server: this.props.server
    });

    this.socketCollection = {};
    this.pingTimer = 2000;

    this.bindListeners();
    this.bindNativeEvents();
    this.initRoomManager();

    this.log(`Ready ${chalk.green("✓")}`);
  }

  log(toBeLogged) {
    if (this.props.log) {
      console.log(`${chalk.grey("[WebSocketServer]")} ${toBeLogged}`);
    }
  }

  bindNativeEvents() {
    this.socket.on('connection', (ws, req) => {
      /* Generate socket ID */
      const id = crypto.randomBytes(8).toString('hex');
      ws.id = id;

      /* Push it to socketCollection */
      this.socketCollection[id] = ws;

      /* Push it to "global" Room */
      this.rm.pushToRoom('global', id);

      this.socketCollection[id].on('pong', this.handlePong.bind(this, id));

      /* Init the ping/pong dynamic */
      this.ping(this.socketCollection[id]);

      /* Handle dynamic Events (conform to protocol { event : XXX, data: XXX }) */
      this.socketCollection[id].on('message', payload => {
        const parsedPayload = JSON.parse(payload);
        this.handle(parsedPayload.event, parsedPayload.data);
      });

      /* Handle the connection (callback set by developer) */
      this.handle('connection', this.socketCollection[id]);

      /* Send socket id to client */
      this.emitTo(this.socketCollection[id], "idGeneration", { id });
    });
  }

  /* Emit an Event to a specified socket */
  emitTo(socket, event, data) {
    socket.send(JSON.stringify({
      event,
      data
    }));
  }

  /* Ping a socket */
  ping(socket) {
    socket.ping(this.handlePingError.bind(this, socket.id));
  }

  /* If we get the pong, we ping again in X seconds */
  handlePong(socketId) {
    setTimeout(() => {
      this.ping(this.socketCollection[socketId]);
    }, this.pingTimer);
  }

  /* If we don't get the pong, we handle the error and kill the connection */
  handlePingError(socketId, err) {
    //todo remove socketId from its room too;
    if (err) {
      this.handle('disconnect', socketId);
      this.socketCollection[socketId].terminate();
      this.socketCollection[socketId] = null;
    }
  }

  initRoomManager() {
    this.rm = new RoomManager({
      log: true
    });
  }

  /* Dynamic on/handle event management */
  // Register an Event and a callback
  on(event, callback) {
    if (!this.eventCollection) {
      this.eventCollection = {};
    }
    this.eventCollection[event] = callback;
  }

  // Handle the Event with the callback (see native socket.on('message', ...))
  handle(event, args) {
    if (this.eventCollection && this.eventCollection[event]) {
      this.eventCollection[event](args);
    }
  }

  /* ----- */

  /* Handles */
  bindListeners() {
    this.on('connection', this.handleConnection.bind(this));
    this.on('disconnect', this.handleDisconnect.bind(this));
    this.on("enterRoom", this.handleEnterRoom.bind(this));
  }

  handleConnection(socket) {
    const { id } = socket
    this.log("new connection : " + id);
  }

  handleDisconnect(socketId) {
    this.log("connexion lost : " + socketId);
  }

  handleEnterRoom(args) {
    const { room, id } = args;
    console.log("handle enter room ", args);
  }

  /* End handles */
}

module.exports = WebSocketServer;